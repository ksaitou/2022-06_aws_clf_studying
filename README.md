# 2022-06 AWS CloudPractitioner (CLF) 資格試験 勉強会

- スライド: https://ksaitou.gitlab.io/2022-06_aws_clf_studying/
- リポジトリ: https://gitlab.com/ksaitou/2022-06_aws_clf_studying

## スライドの作り方

### スライドの開発

```
# 依存性取得
$ yarn

# サーバ: http://localhost:8080
$ yarn start

# HTMLで出力
$ yarn build
```

### 参考

- marp (プレゼンツール) - https://github.com/marp-team/marp-cli
  - https://marpit.marp.app/markdown
