---
title: 2022-06 AWS CloudPractitioner (CLF) 資格試験 勉強会
paginate: true
---

# 2022-06 AWS CloudPractitioner (CLF) 資格試験 勉強会

2022/06 by Kenji Saitou

![Slides are here](images/qrcode.png)

- スライド: https://ksaitou.gitlab.io/2022-06_aws_clf_studying/
- リポジトリ: https://gitlab.com/ksaitou/2022-06_aws_clf_studying

---

# Day 1

1 章・2 章

- 資格概要
- 受験概要
- AWS 概要

---

## 勉強会概要

- AWS の学習のきっかけづくり、教科書の読み合わせ、理解ポイントの共有、情報交換の場の作成が主な目的です
  - ゼロから自分だけで始めるのはキツいと思うので身近な人間が解説する場を設けています
- わからない点は Teams にて聞いてもらって OK （誰かが答えてくれます）
- 教科書は [AWS 認定資格試験テキスト AWS 認定 クラウドプラクティショナー](https://www.amazon.co.jp/AWS%E8%AA%8D%E5%AE%9A%E8%B3%87%E6%A0%BC%E8%A9%A6%E9%A8%93%E3%83%86%E3%82%AD%E3%82%B9%E3%83%88-AWS%E8%AA%8D%E5%AE%9A-%E3%82%AF%E3%83%A9%E3%82%A6%E3%83%89%E3%83%97%E3%83%A9%E3%82%AF%E3%83%86%E3%82%A3%E3%82%B7%E3%83%A7%E3%83%8A%E3%83%BC-%E5%B1%B1%E4%B8%8B-%E5%85%89%E6%B4%8B/dp/4797397403) という本を使います
  - やや古い感じ(2019)がありますが、内容的には今でも通用します
  - どちらかというと、試験対策というよりは基礎力をつける方向性を感じます
- 特に参加制限は設けないです
- 「独学でいける」と思う方は参加しなくても特に問題ないと思います
  - YouTube や公式ドキュメントやトレーニング等教材は豊富にあります

---

## モチベーション

- 日本全国でクラウドインフラエンジニアが不足しています
  - クラウドインフラで設計できる人が足りない
  - クラウドインフラのことを理解している人が足りない
- 本当のインフラは知らなくていい
  - 電気工事の知識や資格は不要
- プロジェクトの条件や制約の上、クラウドインフラにあるコンポーネントを組み合わせて最適なソリューションを作る力が欲しい
  - ソリューションアーキテクトというロール
- AWS は入り口みたいなもので、別のクラウドベンダーを使っても類似の概念は出てくるので覚え損にはならない

---

## CLF (Cloud Practitioner) はどんな資格か？

- AWS の専門用語・基本的な概念を覚える資格
- 11,000 円 / 100 USD
- ベンダー資格
  - 予約が取れればいつでも受験可 / 結果がすぐに分かります
  - 受験すること自体には AWS のアカウントは不要です
- 選択問題　実技なし　日英両方閲覧可能　合格ライン: 700 点 / 1000 点 (90 min)
- 自宅もしくは会場で受験可能（ピアソン VUE or PSI）
  - 2 回まで再予約可能 (3 回目以降はキャンセル)
- 3 年ごとに更新
  - 更新試験無しで同じ試験合格・上位資格とると延長
- 参考: [資格サイト](https://aws.amazon.com/jp/certification/certified-cloud-practitioner/) [料金・受験について](https://aws.amazon.com/jp/certification/policies/before-testing/)

---

## [APN パートナー](https://partnercentral.awspartner.com/home) への登録お願いします

- 以下のサイトより、 **会社のメールアドレスを使用して** アカウント登録を行ってください。
  - https://partnercentral.awspartner.com/APNSelfRegister
- 会社のドメインの入ったメールアドレスをアカウント登録に使うことで会社に自動的に紐づく仕組みです。
- **AWS の利用自体・AWS アカウント とは一切無関係** です。
- **登録した方へのデジタルトレーニング資材の提供**、 **社全体としての資格保持者のカウント**、**他メリット享受** のために行います。
- AWS 認定を既に取得している場合は `AWS トレーニングと認定アカウント E メール` 欄に該当個人メールアドレスを入力してください。また、 `トレーニングと認定同意` では `はい` を選択してください。

---

## [APN パートナー](https://partnercentral.awspartner.com/home) のアカウントを使ってできること

- 資格試験を受ける際のアカウントになります
  - [試験のスケジュールを立てる](https://aws.amazon.com/jp/certification/) → サインイン → `Sign in or Sign up` で `AWS Partner` を選択 → アカウントに移動 → 資格試験のダッシュボード ([certmetrics](https://www.certmetrics.com/amazon/)) → 試験登録
- デジタルコンテンツやトレーニング等を見ることができます
- アイアランスリーダー・チームが許可するユーザのみ
  - AWS のパートナーとしての営業・広報活動 (ACE)
  - AWS からの特典の活用
- 再三いいますが、AWS のアカウント・AWS というサービスの利用と、 APN パートナーは一切何も関係ありません

---

## 受験方法

- 前スライドの[資格試験のダッシュボード](https://www.certmetrics.com/amazon/)より試験登録してください
- 試験の業者（ピアソン VUE or PSI ）を選べます
  - それぞれの中で在宅・もしくは会場を選べます
  - 両者で選べる会場に差異があります
- 今回の試験は `AWS Certified Cloud Practitioner (CLF-C01)` です
- **以前何かしら AWS の資格試験を受験した場合、受験料が半額になるコードが発行されている場合があります。** 該当する人はサイトのメニューの特典を確認してください。

---

## 勉強の仕方

- 資格取得だけなら CLF は **机上学習で取得可能** と言われています
  - ただ、実験用にアカウント作成したほうが理解は進むと思います
  - 最終的に SAA (ソリューションアーキテクトアソシエイト) の取得を目指しましょう
- **今回の教科書＋適当な問題集で問題なく合格できる** と思います
  - 上位資格を持っている場合は無勉強でもいけます

---

## 模擬試験が無制限に受けられます

- [AWS スキルビルダー](https://skillbuilder.aws) にいく
  - APN パートナーアカウントでサインアップできる（追加の情報入力は必要）
- `AWS Certified Cloud Practitioner Official Practice Question Set (CLF-C01 - Japanese)` を検索して選択
- 登録 (Enroll) を選択して開始
  - もし韓国語版が起動するなら URL の `ko-kr` を `ja-jp` に変更すると日本語版が出てくる
- その他いろんなものがスキルビルダーで受けられるのでチャレンジ！！

---

## 参照リンク

- AWS Well-Architected Framework
  - ＝ AWS を使う上での大事な価値観や設計論の話
  - [これ読め](https://docs.aws.amazon.com/ja_jp/wellarchitected/latest/framework/welcome.html)
  - [サイトトップ](https://aws.amazon.com/jp/architecture/well-architected/)
- **デジタルトレーニング**: https://www.aws.training/
- AWS の YouTube チャンネル: https://www.youtube.com/c/JPAmazonWebServices
  - 英語: https://www.youtube.com/c/amazonwebservices/

---

# Day 2

3 章・4 章

- AWS のセキュリティ
- AWS のテクノロジー (リージョンの説明など)

---

## 3 章 〜 AWS アカウント

- プレーン = plane = 「層」
- AWS におけるアカウント = メールアドレスで取得するもの = AWS を利用する上での箱
  - `1` AWS アカウント <=> `1` メールアドレス <=> `1` ルートアカウント
    - **IAM ユーザーとは全然違う概念** なので注意
  - AWS アカウント内にいろんなリソースを作成して環境を作っていく
    - AWS アカウントで設定した請求先に毎月請求が来る
  - 他のクラウドだとプロジェクト (GCP) や リソースグループ (Azure) があるが、AWS は無いので、マルチアカウントで構成していく
    - AWS Organizations でツリー状にマルチアカウントを整理することが可能

---

## 3 章 〜 IAM (アイアム)

- 要するに、その AWS アカウント内の認証・認可関連のことだと思えば OK
- 認 **証** と認 **可** は違う概念です
  - 認証 Authentication
    - あなたは誰か **証** 明してください （対象の特定）
    - 例: パスワード認証で A さんを特定する
  - 認可 Authorization
    - 誰にどんな操作を許 **可** するか？
    - 例: A さんに管理者権限を認める

---

## 3 章 〜 AWS における 認証 (Authentication): あなたは誰？

- **ルートアカウント**: 権限強すぎ 普段使いするな
  - コンソールでのログイン
- [**IAM ユーザー**](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_users.html): 一番よく使う
  - コンソールでのログイン
    - 2 要素認証等を設定可能
  - アクセスキー・シークレットキー (要するに ID/パスワード)でのログイン
    - AWS SDK / AWS CLI で使う
  - **IAM グループ**: IAM ユーザーをまとめて権限付与したりなど
- [**IAM ロール**](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_roles_terms-and-concepts.html): 凝った設定で使う
  - 特定の AWS サービス
    - 特に S3 はいろんな AWS サービスのファイル置き場になるので頻出
  - よそのアカウント・よその認証基盤のユーザー

---

## 3 章 〜 AWS における 認可 (Authorization): 誰にどんな操作を許可するか？

- AWS における「認可」は ↓ 基本これ！
  - 「`Principal`: 誰が」
  - 「`Resource`: 何に対して」
  - 「`Action`: 何をする」
- … を「`Effect`: 許す(`Allow`)・禁じる(`Deny`)」
- 自然言語の SOV (主語・目的語・動詞) と同じです
- 例: 「 `ユーザーA` が」 「`S3のバケット hoge` に対して」「`読み書き` する」 ことを `許す`
- この「認可」を集めたものを **IAM ポリシー** といいます
  - JSON で書きます（[例](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/reference_policies_evaluation-logic.html#policies_evaluation_example)）

---

## AWS で暴走したユーザーからインフラを守る手段はほとんどないので、適切に権限をコントロールしてください

- 環境ごとにアカウント分ける
- 最低限度の権限を切る
- 利用料金の把握・統制
- AWS の勉強を行いたい場合は、実験用にアカウント切りましょう
  - 作った資材がいつ消されても文句ない場所を作る
- **作ったアカウントは覚えておく**

---

# Day 3

5 章・6 章

- EC2 などワークロード処理系
- EBS などデータストア

---

## EC2 = VirtualMachine （仮想マシン）

- AWS の虚無ワード「Amazon」「AWS」「Elastic」「Cloud」「Virtual」
  - ケースバイケース・費用対効果ぐらい意味のないワード
- AZ に立てる
- インスタンス = イメージ（↓）から起動した VM

## AMI = VM のイメージ （雛形）

- 内部的には S3 に入る
- 手で作ってもいいですが、 [Packer](https://www.packer.io/) というツールもあります

---

## 転送料金

- データ転送自体への料金は EC2 に限らずかかる。
- まず下記が原則: [参考](https://aws.amazon.com/jp/blogs/architecture/overview-of-data-transfer-costs-for-common-architectures/)
  - → Ingress（インターネットから AWS への入力）無料
    - CloudWatch Logs などデータ流入の料金とは別
  - ← Egress（AWS からのインターネットへの出力）有料
  - 「来るもの拒まず、去るもの追う」
- AWS 内
  - リージョンをまたぐと有料
  - 別 AZ 間での通信は有料
  - 同一リージョンのサービスエンドポイントは無料
- 細かい条件で通信料金は違う
- 基本的に [AWS は転送料金が高いという批判](https://blog.cloudflare.com/aws-egregious-egress/)があります

---

## EC2 と名前は出てないけど、EC2 で出来ているサービスがたくさんありそうな件

- EC2 は我々にとって、そして **AWS 自身にとっても基本的なサービスです**
- AWS のサービスも割と EC2 を下地に構築していると思われます
  - 特に公表はされてないが……
  - 基本的に EC2・S3・DynamoDB で出来ている気がする

---

## ELB - ロバラ（ロードバランサー）

- E(lastic)LB = A(pplication = HTTP)LB + N(etwork = TCP)LB + C(lassic)LB
  - CLB は使うことはない
- 下記があれば、だいたいのシステムは作れる（＝昔からある）
  - EC2 (VM)
  - ALB(ロバラ)
  - RDS(データベース)
  - S3(ファイル・バックアップ置き場)
  - (VPC などネットワーク)
- VPC に立てて、トラフィックを流す先の AZ (サブネット)を選ぶ

---

## スケーリング (sclaing)

- 水平スケーリング（＝台数増やす: スケールアウト・スケールイン）
- 垂直スケーリング（＝強くする: スケールアップ・スケールダウン）
- クラウドの常識: 水平スケーリング ＞＞＞ 垂直スケーリング
  - とはいえ、水平にも限度があるという主張: [アムダールの法則](https://ja.wikipedia.org/wiki/%E3%82%A2%E3%83%A0%E3%83%80%E3%83%BC%E3%83%AB%E3%81%AE%E6%B3%95%E5%89%87)

---

## サーバは役割で分けましょう

- サーバはアプリケーションサーバとデータストア（DB 等）を必ず分けましょう
  - 前者は水平スケーリングで増やせるようにしましょう
    - ステートレス＝イミュータブルインフラストラクチャ
  - 後者は多くは垂直スケーリングで対処することになります
    - 特に書き込む方はスケールしません
    - 読み込む方は通常は水平スケールできます
- データストアは管理が難しいので、マネージドサービスに頼りましょう (例: Amazon RDS)
  - アプリケーション: 自分で作る
  - データストア: 詳しい人が作ったものを使う

---

## AWS Lambda

- Lambda = ラムダ = Λ = ラムダ計算 = 転じて計算中に現れる小さな関数
  - もともとは数学で Λ みたいな記号を使っていたのを、ギリシア文字のラムダに見立てた…　みたいな語源だった気がする
- いわゆるサーバレスの代表格
  - サーバがないわけではない　所有・意識しなくていいということ
  - OS やらの保守が不要・リクエスト数に応じた無限スケーリング

---

## AWS Lambda

- なんのために使うか？
  - システムの API として
    - 世間的にはこっちが多いと思われがちだが、
  - AWS 内のちょっとした「マクロ関数」として
    - こっちのほうが多い
    - AWS のサービスのログが溜まった時の加工関数とか、サービスログイン可否を決める関数とか
- 内容的にはイベントで呼ばれるコールバック関数 (main 関数) を実装するだけ

```js
exports.handler = async function (event, context) {
  console.log("EVENT: \n" + JSON.stringify(event, null, 2));
  return context.logStreamName;
};
```

---

## EBS (VM につける SSD や HDD)

- ネットワークを経由して VM にアタッチされる
  - いわば NAS ですね
  - VM と同一 AZ にある必要がある
- 基本的に EC2 のディスクといえば、全てこれを使います
- ブロックストレージ(block storage)とは何か？
  - OS 等がデータ位置(ブロック位置)を指定して読み書きできるストレージのことです。要するに普通のマシンにささってるディスク。
  - 対義語
    - file storage: NFS
    - object storage: ウェブのドライブシステムとかファイル単位でしか保管できないやつ

---

## インスタンスストア (ローカルのディスク、EBS じゃないほう)

- 「 **便利で速い** けど」「**栄養にならない**（終了するとデータが残らない）」「**インスタント麺**」で覚えれば OK
- EBS が NAS なのに比較して、[AWS のデータセンター上で物理的に VM にぶっささってるストレージ](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/InstanceStorage.html)です。要するに普通のマシンの SSD。
- VM を停止 → 再開すると、おそらく他の同スペックの代替の VM が使われるので、再度そのストレージにアクセスできないから消えるよ、という理屈だと思います
  - そういう理由により、AWS 側の都合でデータが消える
- 普通は使う必要ありません
  - ハイスペックな EC2 インスタンスにしか生えてません
  - 機械学習等でよく使うデータを入れておくなどといった用途が考えられます

---

## クラウドで性能を出すには？

- とにかく局所性（近さ）だ！
  - 同一リージョンにする
  - 同一 AZ にする
  - 同一 [プレースメントグループ](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/placement-groups.html) にする（マシン間の遅延が減る）
  - インスタンスストアを使う
  - プログラムで配列を使う（メモリアクセス上の局所性が高くて遅延が減る）
  - キャッシュを使う
    - クライアント上でキャッシュする
    - クライアントに近い位置でキャッシュする（CDN: CloudFront）
    - サーバ上でキャッシュする（ElastiCache）
- ↑ は極論ですが、近いと速いのは事実（遠いと耐障害性が増す傾向）

---

## 超重要 S3 (Simple Storage Service)

- EBS との違い
  - **動** EBS: よく書き換わるデータの置き場
    - AZ 単位で存在 (VM と同一 AZ)
    - 密結合: OS のファイルアクセス
  - **静** S3: ファイル (オブジェクト) を CRUD するだけのデータ置き場
    - リージョン単位でサービスが存在 (別リージョンでも問題なし)
    - 疎結合: HTTPS で API を叩いてアクセス
- 耐久性（Durability）がイレブンナイン (`99.999,999,999` %)
  - 10^11 (1000 億) 個ファイルがあるとすると、年 1 個消失する
  - Azure (Blob Storage ZRS)だと twelve-nine
- とにかくここに入れておけば安心

---

## S3

- DB で管理しない大きいファイルデータは S3 に入れる
  - OS のファイルシステムに入れない
  - どうしてもそうしたいなら EFS (AWS の NFS を提供するサービス) を使う
- ほか、いろんな AWS のサービスがデータ置き場に使う
  - ログ、バックアップ等
- 昔学んだ人へ: 結果整合性 (eventual consistency) は緩和されました
  - 昔は書き込んだ後にすぐに内容が反映されないとかあったのですが、[そういうのは今は無い](https://aws.amazon.com/jp/s3/consistency/)らしい
- 内部的な仕組み
  - リージョン内のどこかの AZ に書き込むと他のリージョン内にコピー
    - 冗長性等の詳細はストレージクラスで違う

---

## 大手クラウドプラットフォームを使う嬉しさは何か？

- 分散系 (distributed systems) をきちんと作っているところ
  - 複数のマシンが組み合わさってできる系
  - 何が故障するか分からないから、どれが故障してもいいようにしてある
- 故障した場合にきちんとフェールセーフになる設計はかなり難しい
  - e.g., [split-brain](https://ja.wikipedia.org/wiki/%E3%82%B9%E3%83%97%E3%83%AA%E3%83%83%E3%83%88%E3%83%96%E3%83%AC%E3%82%A4%E3%83%B3%E3%82%B7%E3%83%B3%E3%83%89%E3%83%AD%E3%83%BC%E3%83%A0), [結果整合性](https://ja.wikipedia.org/wiki/%E7%B5%90%E6%9E%9C%E6%95%B4%E5%90%88%E6%80%A7), [合意形成](https://ja.wikipedia.org/wiki/Paxos%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0) ([Raft](https://raft.github.io/)), [そういうのを調査してる人](https://jepsen.io/)
  - [TLA+](https://ja.wikipedia.org/wiki/TLA%2B) などを使って分散系を証明（開発）しています
  - こういった開発は通常の企業には難しい

---

## AWS におけるデータストアの暗号化 (TDE) とは何か

- 別に使う側は暗号化されているかは分からない
  - AWS が実際に保存する媒体の上で自動で暗号化し、使う時に自動で復号化するだけ（TDE: 透過的データ暗号化）
- 別にユーザーレベルのデータ漏えいには役に立たない
  - AWS からのデータ漏えいがあった時に役に立つ
    - ストレージが手順に従わずに廃棄された場合
    - データセンターが襲われた場合
  - コンプラでディスク上の暗号化を求めている場合にも役に立つ
- KMS であれば、キーを蒸発させれば誰も復号化できなくなる
  - 最近のスマホもそうですね

---

## シンプルなサーバーを安く立てたいだけなら EC2 より [Amazon Lightsail](https://aws.amazon.com/jp/lightsail/pricing/) がおすすめ

- 予約 = リザーブドインスタンス不要
  - 時間ごと課金 ⇔ 他社は月額・年額
- 国内ライバル社の最低価格並み
  - 3.5 USD/mo: 512MB/1vCPU/20GB 〜 ⇔ [例: さくら 643 円/mo 〜](https://vps.sakura.ad.jp/specification/)
  - 普通のサイトなら十分な転送量付き
- EC2 同様すぐにプロビジョンできる
  - 実質的な中身は EC2 の `t` ファミリーインスタンス
- 他社よりアパートメントが優れているので迷惑な他テナントに妨害される心配低
- 細かいカスタマイズや EC2 のような高機能性は無い

---

# Day 4

7 章〜10 章　最後まで

- ネットワーク
- データベース
- 管理サービス
- 請求

---

## VPC = 要するに LAN

- LAN (Local Area Network) を作る機能
- リージョンの上で VPC (LAN)を切り
  - それぞれ AZ 上でサブネット (CIDR)を切る
- インターネットゲートウェイ
  - LAN の上のインターネットルーターだと思えば OK
  - これを繋がないとインターネットとはつながらない
- サブネット
  - VPC × AZ で作れるもの
  - ルートテーブル: どの IP アドレス宛のパケットをどのマシンに送るかのルール

---

## VPC - ネットワーク ACL、セキュリティグループと何が違うのか

- どっちもファイアウォールみたいなもの
- セキュリティグループ
  - マシンに設定する
  - ステートフル (TCP で一度確立したコネクションは通す)
    - [極端な環境](https://d1.awsstatic.com/events/jp/2018/summit/tokyo/customer/06.pdf)だと性能の上限が出る
  - 普段使いはこちら
- ネットワーク ACL
  - サブネットに設定する
  - ステートレス (特にコネクションの確立は見ない)
  - ネットワークポリシーを作るならこちら

---

## VPC ピアリング

- VPC を跨ぐと直接通信できないので、つなげられます
  - VPC を複数個つなぐ場合は推移性 (transitivity) を持たせてね
    - 推移性 = 友達の友達は友達
- 制限が多いので最近（？）できた [Transit Gateway](https://aws.amazon.com/jp/transit-gateway/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc) が推されることが多い

## CloudFront - 要するに CDN

- [Cloudflare](https://www.cloudflare.com/ja-jp/) に次いで利用者が多い CDN
- ACM (AWS Certificate Manager)
  - SSL/TLS 証明書発行してくれるやつ
  - AWS のサービス向けの証明書は発行無料(!)

---

## Route53 - DNS って何?

```
== Root DNS
www.asarepo.com は ↓ のゾーンが答えます

== com
www.asarepo.com は ↓ のゾーンが答えます

== asarepo.com
www.asarepo.com は ↓ のゾーンが答えます

== www.asarepo.com
A www.asarepo.com => 49.143.244.156
```

- ゾーンの直下(頂点 = Apex)には `CNAME` を作れないがそれを緩和する機能が Route53 にある
- メールサーバとともに自分で立ててはいけない四天王(?)の一角

---

## SES - メール送信サービス

- [Amazon SES](https://aws.amazon.com/jp/ses/) = Simple Email Service
  - 本には出てこないが、普通に使うので紹介
- リージョンごとのサービス
  - 東京リージョンは長らくなかったけれど、最近は使えます
- [デフォルトでは EC2 インスタンスや Lambda からのポート 25 からのデータ送信はブロックされてます](https://aws.amazon.com/jp/premiumsupport/knowledge-center/ec2-port-25-throttle/)
  - スパマー対策と思われる。個別に緩和申請すれば解除できる。
- 代替サービス
  - [SendGrid](https://sendgrid.com/) / [Mailgun](https://www.mailgun.com/)

---

## RDS - データベース(RDBMS)

- MySQL / PostgreSQL / SQLServer ほか / **Aurora**
  - Aurora: Amazon が独自開発した高いけど性能が出る DB
    - Aurora MySQL
    - Aurora PostgreSQL
    - Aurora Serverless
      - 最近登場した ↑ のスケーリングがすごい DB

## DynamoDB - AWS 独自の KVS

- RDB とかと全然違う、AWS 独自の KVS(キーバリューストア) / NoSQL の一種

---

## CloudWatch - 監視・アラート・イベント・料金監視

- Watch = 「監視」
- ログ監視・イベント通知等いろんなサービスがあるが、それの **総称**

## CloudTrail - AWS アカウント内の AWS 操作に関するログ

- Trail = 「痕跡」
- そもそもあらゆる AWS 上の CRUD 動作は AWS の REST API サーバに決まった方法で JSON 等を送りつけて実現していて、それのログ（記録）が CloudTrail で見ることができる
  - AWS の画面称マネコン = Management Console)も API サーバを叩いてるだけ
  - 直接 AWS の API を叩きたい人は [こちら](https://docs.aws.amazon.com/general/latest/gr/aws-apis.html) 参考
- ↑ CloudWatch ととにかく混同しやすい
  - **AWS 上で不正な操作が行われていないかチェックするのはだいたいこっち**

---

## 請求

- 請求の閲覧や操作はデフォルトではルートアカウントしかできない
  - 明示的にルートアカウントが IAM ユーザ等に許可しないと閲覧すら不可
- 請求が膨らむ原因
  - 作ったものの消し忘れ・退職
    - (例) リソースの所有者が分からなくて消していいか分からない
  - そこに課金されるの？
    - (例) [CloudWatch Logs](https://aws.amazon.com/jp/cloudwatch/pricing/) は流入量に一番課金されます（保管料ではない）
  - 毎月ちょっとずつ利用料金があがり、気づいたら……
    - (例) システム利用人数増量 / データ保管容量上昇
  - → 統制・監視がないのが原因
    - → 基本的に管理者しか料金を把握しないがちなのも原因

---

## その他

- 本勉強会では教科書をベースに付加知識を伝えてきました
  - どちらかというと実践寄りで試験範囲とは無関係なものが多いですが、実運用上の空気感は分かるかなと思います
- 他資格は、ここで学んだことが広く・深く問われるだけです

# 本日までお疲れさまでした　試験頑張りましょう

---

# おわり
